> :warning: **Warning:** This documentation refers to the [proof-of-concept implementation of Wildland client](https://gitlab.com/wildland/wildland-client) which is no longer maintained. We are currently working on a new Wildland client written in Rust. To learn more about Wildland and the current status of its development, please visit the [Wildland.io webpage](https://wildland.io).

# Integrations

Intergation backends allow the user to easily expose and incorporate data from any third party source into their filesystem. Therefore, they are primarly meant to function as a data source as opposed to the [storage-exposing backends][storage-backends]. At the moment, the following integration backends and their corresponding guides are available:

| BACKEND      | HOWTO                  |
| ------------ | ---------------------- |
| Transpose      | [HOWTO][transpose-howto] |

The full list of all currently supported plugins (both integration and storage backends) can be obtained using the `wl template create` command.

```console
$ wl template create
Usage: wl template create [OPTIONS] COMMAND [ARGS]...

  Creates storage template based on storage type.

Options:
  --help  Show this message and exit.

Commands:
  bear-db           Create bear-db storage template
  categorization    Create categorization storage template
  date-proxy        Create date-proxy storage template
  delegate          Create delegate storage template
  dropbox           Create dropbox storage template
  dummy             Create dummy storage template
  encrypted         Create encrypted storage template
  git               Create git storage template
  gitlab            Create gitlab storage template
  gitlab-graphql    Create gitlab-graphql storage template
  googledrive       Create googledrive storage template
  http              Create http storage template
  imap              Create imap storage template
  ipfs              Create ipfs storage template
  local             Create local storage template
  local-cached      Create local-cached storage template
  local-dir-cached  Create local-dir-cached storage template
  pseudomanifest    Create pseudomanifest storage template
  s3                Create s3 storage template
  sshfs             Create sshfs storage template
  static            Create static storage template
  transpose         Create transpose storage template
  webdav            Create webdav storage template
  zip-archive       Create zip-archive storage template

```

[transpose-howto]: ./transpose-backend.md
[storage-backends]: ../storage-backends/index.md
