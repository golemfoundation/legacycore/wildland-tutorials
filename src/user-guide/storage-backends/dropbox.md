# Dropbox Plugin

This tutorial explains how to use the Wildland Dropbox plugin which allows you to store and manage your files kept in your Dropbox account through Wildland.

Before reading the following tutorial, it is recommended to get familiar with the [fundamentals of Wildland][practical-into-to-wildland].

## Generating a Dropbox app key

The Wildland Dropbox plugin allows you to use Dropbox as read/write storage for Wildland containers. To create Dropbox storage backend, you need to generate a Dropbox App Key which will be passed as a `--app-key` parameter Wildland CLI when creating storage (with `wl storage create`) or storage template (with `wl storage-template create dropbox`).

To generate a Dropbox app key, follow these steps:

1. Navigate to the [Dropbox App Console][dropbox-dev-apps-console] and select `Create app`.

   ![Create app](../../assets/screenshots/dropbox/dropbox-create-app.png)

2. In the `Choose an API` section select `Scoped access` (which is the only option available at the time of writing this tutorial). To give access to all files stored in your Dropbox account, select `Full Dropbox` access. To create a directory which will be dedicated solely to Wildland, use the `App folder` instead. The name of the app is an arbitrary unique name which will be displayed on the [Dropbox App Console][dropbox-dev-apps-console] landing page. Click on `Create app` to submit the selected settings.

   ![Choose an API: Scoped access](../../assets/screenshots/dropbox/dropbox-create-app-initial-settings.png)

3. Navigate to the `Permissions` tab and select the following permissions from the `Files and folders` section:

   - `files.metadata.write`
   - `files.metadata.read`
   - `files.content.write`
   - `files.content.read`

   ![Permissions tab](../../assets/screenshots/dropbox/dropbox-permissions-settings.png)

4. Make sure to apply the changes by clicking on the `Submit` button.

   ![Submit permissions](../../assets/screenshots/dropbox/dropbox-permissions-change-successful.png)

5. Copy `App Key`. You will use to create Dropbox storage for your Wildland container.

## Creating Dropbox storage

Create a Dropbox storage template with the following command:

```console
$ wl template create dropbox mydropbox \
    --app-key YOUR_DROPBOX_APP_KEY
```

You will be asked to open a URL. Open this URL to login your Dropbox account and then, allow the request. Finally, copy and paste the generated access code inside the console.

Once above process is complete you will be able to create storage container as follows:

```console
$ wl container create dropbox-container \
    --template mydropbox \
    --path /dropbox
```

## Mounting

The mounting process is no different than that of any other container.

```console
$ wl start
$ wl c mount dropbox-container
```

You are now ready to access your Dropbox account using Wildland! To list your files, run:

```console
$ ls -la ~/wildland/dropbox
```

[practical-into-to-wildland]: ../../index.md
[dropbox-dev-apps-console]: https://www.dropbox.com/developers/apps
