> :warning: **Warning:** This documentation refers to the [proof-of-concept implementation of Wildland client](https://gitlab.com/wildland/wildland-client) which is no longer maintained. We are currently working on a new Wildland client written in Rust. To learn more about Wildland and the current status of its development, please visit the [Wildland.io webpage](https://wildland.io).

Currently only the Linux platform is supported directly. However, we provide a handy Docker packaging for the client, allowing the user to run it on other platforms as well. Check the following tutorials for a step-by-step guide on how to run Wildland on:

- [Linux][linux-howto]
- [macOS][macos-howto]
- [Windows 10][windows-howto]

[linux-howto]: ./linux.md
[macos-howto]: ./macos.md
[windows-howto]: ./windows.md
