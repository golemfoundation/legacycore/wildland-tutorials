> :warning: **Warning:** This documentation refers to the [proof-of-concept implementation of Wildland client](https://gitlab.com/wildland/wildland-client) which is no longer maintained. We are currently working on a new Wildland client written in Rust. To learn more about Wildland and the current status of its development, please visit the [Wildland.io webpage](https://wildland.io).

# MacOS

## Run Wildland client from a Docker image

### Install Docker

Using the Homebrew package manager (if you don't have Homebrew, install it following the steps described on their [home
page](https://brew.sh)):

```
% brew install --cask docker
```

Launch the Docker.app and let it finish the installation.

### Clone Wildland repository

```
% git clone -b master https://gitlab.com/wildland/wildland-client.git
```
### Build and start Docker image

```
% cd wildland-client/docker
% docker-compose build
% docker-compose run --service-ports wildland-client
```

This will start the Wildland terminal session through which you can
interact with Wildland client using the `wl` command.

To connect to the Wildland filesystem using Finder, select _Connect to Server_ from _Go_ menu and type `http://localhost:8080` as
the server address. If you get a warning about an insecure connection, just choose to connect and select "Connect as Guest" when asked
for a password.

Once you have completed all of these steps, head over to the [Quick Start Guide][quick-start] and follow the instructions there.

[quick-start]: ../quick-start.md
