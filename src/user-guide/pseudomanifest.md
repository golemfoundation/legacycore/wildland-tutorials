> :warning: **Warning:** This documentation refers to the [proof-of-concept implementation of Wildland client](https://gitlab.com/wildland/wildland-client) which is no longer maintained. We are currently working on a new Wildland client written in Rust. To learn more about Wildland and the current status of its development, please visit the [Wildland.io webpage](https://wildland.io).

# Pseudomanifests

A pseudomanifest is a file that can be found in the root directory of a mounted container.
It allows users to distinguish directories that are containers from directories that are mere 
directories.
Moreover, we can easily grasp all the paths/categories this container is exposed at.

## How to edit a pseudomanifest

The pseudomanifest can be edited as a plain text file, but not all modifications are accepted.
We can only change paths, categories and the title.
Saving the modified pseudomanifest applies the changes to the original container manifest, 
and then republishes and remounts it.

### Where pseudomanifests can be found

Let's create a new container with dummy storage and mount the Wildland file system.

```console
$ wl container create MyContainer --path /PATH
$ wl storage create dummy --container MyContainer
$ wl start
```

Now we should be able to see pseudomanifests. 
In this example, all the `.manifest.wildland.yaml` files are the same pseudomanifest 
because we only have one container here.

```console
$ tree -a ~/wildland/
/home/user/wildland/
├── .backends
│   └── 89453c86-64a9-4d27-a8ab-cd4d6a8242f4
│       └── c4d1757f-fdb1-4db3-abaa-4096818535aa
│           └── .manifest.wildland.yaml
├── .users
│   └── 0xce37e2172b827ab04f53d8672ea52980e475832ff8948db529a960b26da132cc:
│       ├── .backends
│       │   └── 89453c86-64a9-4d27-a8ab-cd4d6a8242f4
│       │       ├── c4d1757f-fdb1-4db3-abaa-4096818535aa
│       │       └── c4d1757f-fdb1-4db3-abaa-4096818535aa-pseudomanifest
│       │           └── .manifest.wildland.yaml
│       ├── .uuid
│       │   └── 89453c86-64a9-4d27-a8ab-cd4d6a8242f4
│       │       └── .manifest.wildland.yaml
│       └── PATH
│           └── .manifest.wildland.yaml
├── .uuid
│   └── 89453c86-64a9-4d27-a8ab-cd4d6a8242f4
│       └── .manifest.wildland.yaml
└── PATH
    └── .manifest.wildland.yaml
```

Let's see the contents of the pseudomanifest file.
It is identical to the original container manifest, except that the backend section is omitted.

```console
$ cat ~/wildland/PATH/.manifest.wildland.yaml
object: container
owner: '0xce37e2172b827ab04f53d8672ea52980e475832ff8948db529a960b26da132cc'
paths:
- /.uuid/89453c86-64a9-4d27-a8ab-cd4d6a8242f4
- /PATH
title: 'null'
categories: []
version: '1'
access: []
```

### Editing a pseudomanifest

The pseudomanifest can be modified as a plain text file. 
We can edit the pseudomanifest under any path and the changes will be applied to the other paths, too.

```console
$ vim ~/wildland/PATH/.manifest.wildland.yaml
```

Let's change the pseudomanifest as follows:

```
object: container
owner: '0xce37e2172b827ab04f53d8672ea52980e475832ff8948db529a960b26da132cc'
paths:
- /.uuid/89453c86-64a9-4d27-a8ab-cd4d6a8242f4
- /NEWPATH
title: new_title
categories:
- /cat
version: '1'
access: []
```

We changed the name of `/PATH` to `/NEWPATH`, added a new category `/cat`, 
and set the title to `new_title`.
These changes were applied to our Wildland file system when the file was saved.
Note that the `~/wildland/PATH/.manifest.wildland.yaml` path no longer exists.

```console
$ tree -a ~/wildland/
/home/user/wildland/
├── .backends
│   └── 89453c86-64a9-4d27-a8ab-cd4d6a8242f4
│       └── c4d1757f-fdb1-4db3-abaa-4096818535aa
│           └── .manifest.wildland.yaml
├── .users
│   └── 0xce37e2172b827ab04f53d8672ea52980e475832ff8948db529a960b26da132cc:
│       ├── .backends
│       │   └── 89453c86-64a9-4d27-a8ab-cd4d6a8242f4
│       │       ├── c4d1757f-fdb1-4db3-abaa-4096818535aa
│       │       └── c4d1757f-fdb1-4db3-abaa-4096818535aa-pseudomanifest
│       │           └── .manifest.wildland.yaml
│       ├── .uuid
│       │   └── 89453c86-64a9-4d27-a8ab-cd4d6a8242f4
│       │       └── .manifest.wildland.yaml
│       ├── NEWPATH
│       │   └── .manifest.wildland.yaml
│       └── cat
│           └── new_title
│               └── .manifest.wildland.yaml
├── .uuid
│   └── 89453c86-64a9-4d27-a8ab-cd4d6a8242f4
│       └── .manifest.wildland.yaml
├── NEWPATH
│   └── .manifest.wildland.yaml
└── cat
    └── new_title
        └── .manifest.wildland.yaml
```

### Editing a pseudomanifest - troubleshooting

Not all changes to the pseudomanifest are accepted, for example let's try to change the pseudomanifest as follows:
```
object: container
owner: '0xce37e2172b827ab04f53d8672ea52980e475832ff8948db529a960b26da132cc'
paths:
- /NEWPATH
title: new_title
categories:
- /cat
version: '1'
access: []
```

When we try to save a file in vim, we may see the following warning:

```
"wildland/NEWPATH/.manifest.wildland.yaml" E512: Close failed
WARNING: Original file may be lost or damaged
don't quit the editor until the file is successfully written!
```

We can ignore this and close the file without saving.
If for some reason saving the pseudomanifest file fails, 
we can reopen the file and in the comments below the content we will find 
the rejected pseudomanifest, followed by an explanation of why it was rejected.


```console
$ cat ~/wildland/NEWPATH/.manifest.wildland.yaml
object: container
owner: '0xce37e2172b827ab04f53d8672ea52980e475832ff8948db529a960b26da132cc'
paths:
- /.uuid/89453c86-64a9-4d27-a8ab-cd4d6a8242f4
- /NEWPATH
title: new_title
categories:
- /cat
version: '1'
access: []

# Changes to the following manifest
# was rejected due to encountered errors:
#
# object: container
# owner: '0xce37e2172b827ab04f53d8672ea52980e475832ff8948db529a960b26da132cc'
# paths:
# - /NEWPATH
# title: new_title
# categories:
# - /cat
# version: '1'
# access: []
# 
# 
#  Pseudomanifest error: uuid path cannot be removed.
```
