# Cafe Forest HOWTO

Cafe fingerprint:
```
0xcd5ad6ed64f28561134818db641d475dbcf43fff7a4517c9cdb1658901842579
```

This is internal-use-only HOWTO for the new "Cafe Forest". This new forest is intended for sharing various resources not directly related to Wildland, Golem Foundation, or even Computer Engineering. Share interesting articles, art, etc. Please make sure to tag the shared resources appropriately, so that others can find them. Of course we want to make use of the Wildland container's title/categories features for this. Instructions below provide simple examples.



## Access to Cafe and Admin contact

The Cafe forest user manifest is encrypted to a select group of pubkey, so in order to be able to find, mount and use it, one needs to send their pubkey to the Cafe Forest Administrator. After the administrator accepts the request, she will update the user manifest of the Cafe forest to include the new pubkey, and re-encrypt all the manifests exposed by the cafe forest.

Cafe forest administrator: `admin@wildland.io`

## Finding and Mounting the Cafe forest

1. Mount the kartoteka forest:

    ```console
    $ wl forest mount :/mydirs/kartoteka:
    ```

1. Navigate to the mounted kartoteka and found the bridge leading to the cafe forest, then mount it (go and look around for yourself, do not blindly copy-paste these examples, as the actual paths might differ):

    ```console
    $ wl forest mount :/mydirs/kartoteka:/forests/cafe:
    ```

    Note: the clumsy `*`-syntax is no longer needed to mount a forest (assuming you have the latest Linux client), but it still could be used, if one so desires. Note that `*`-syntax is used via `wl container mount`, while the new syntax makes use of the `wl forest mount`.



## Create storage templates for use with your containers

Unlike Pandora forest, the Cafe forest does not provide its users with storage. This means each user, who would like to post something to the Cafe, should do so on their own storage. The easiest way to do so is with the help of storage templates.

### Storage using Fastmail WebDAV

Note that Fastmail WebDAV storage we have doesn't offer an option for r/o access -- we thus can only create full-access storage manifests. This is a limitation of Fastmail, not Wildland.

```console
$ wl storage-template create webdav fastmail-files \
    --login <YOUR_FASTMAIL_USERNAME> \
    --password <YOUR_FASTMAIL_APP_PASSWORD> \
    --url https://webdav.fastmail.com/<YOUR_FASTMAIL_USERNAME>.wildland.io/files/ \
    --access 0xcd5ad6ed64f28561134818db641d475dbcf43fff7a4517c9cdb1658901842579
```

NOTE: Do not use Fastmail's account password, but instead head over to app-passwords and generate a dedicated password for File access! Also, replace `@` in username in the `--url` with `.` (e.g. `joanna.wildland.io` instead of `joanna@wildland.io`). These are Fastmail-specific considerations, unrelated to Wildland.

NOTE: the `--access 0xcd5ad6ed64f28561134818db641d475dbcf43fff7a4517c9cdb1658901842579` switch adds the `access:` clause to the created template which will cause any storage created out of it to be encrypted to the `cafe` (group) user by default.

If the particualr WebDAV server supported also read-only mode of access, then it would make sense to have two templates:

```console
$ wl storage-template create webdav mywebdav \
    --login <USERNAME_FOR_RW_ACCESS> \
    --password <PASSWORD_FOR_RW_ACCESS> \
    --url <WEBDAV_URL> \
    --access <you>

$ wl storage-template create webdav mywebdav \
    --login <USERNAME_FOR_RO_ACCESS> \
    --password <PASSWORD_FOR_RO_ACCESS> \
    --url <WEBDAV_URL> \
    --access 0xcd5ad6ed64f28561134818db641d475dbcf43fff7a4517c9cdb1658901842579
```

This would result in the `mywebdav` template to actually comprise of two templates -- one for R/W access, and another for R/O access. Any R/W storage backend created out of this template would be then encrypted to the `<you>` user (replace with your username), thus allowing only that one user for R/W access to the resource, while others for R/O access only. It is possible to give multiple `--access` option, allowing select set of users. The user can also be a group user, such as Pandora.

Always make sure to have the R/W template before the R/O template, as Wildland would use the first available for access.

### Storage using Dropbox folder

Assuming you're logged in to your Dropbox account, navigate to `https://www.dropbox.com/developers/apps` and create a new app with App-folder access permissions. Name it e.g. `cafe-storage`. Select "No expiration" for the token type on the next screen, adjust permissions to allow write-access on the next tab, and click the `Generate` button to obtain the access token string. Now, to create a Wildland storage template:

```console
$ wl storage-template create dropbox cafe-dropbox --token <YOUR_DROPBOX_ACCESS_TOKEN> \
    --access 0xcd5ad6ed64f28561134818db641d475dbcf43fff7a4517c9cdb1658901842579
```

By default this token will allow for read-only access to your App's dedicated folder on your Dropbox.

### Storage using Amazon S3 bucket (for power users)

Please see this ticket for instructions on how to setup a dedicated S3 bucket on AWS for use as Wildland storage:
https://gitlab.com/wildland/wildland-client/-/issues/352

`TODO`: turn this into a standalone document and place in the same dir as this HOWTO.

Typically we would need two storagae templates:

1. one with full access rights to the storage -- this is for us to be able to modify the container content,
1. one with r/o access rights -- this would be for others to read our container's content.

These templates can be grouped neatly together inside one template (as described above in the WevDAV section):

```console
$ wl storage-template create s3 my-aws-bucket \
    --access-key <ACCESS_KEY_RW> --secret-key <SECRET_KEY_RW> \
    --s3-url s3://my-bucket-id \
    --access <you>

$ wl storage-template create s3 my-aws-bucket \
    --access-key <ACCESS_KEY_RO> --secret-key <SECRET_KEY_RO> \
    --s3-url s3://my-bucket-id \
    --access 0xcd5ad6ed64f28561134818db641d475dbcf43fff7a4517c9cdb1658901842579
```

Always make sure to have the R/W template before the R/O template, as Wildland would use the first available for access.



## Creating and publishing containers

In case of a Cafe forest there is no need to use of delegated containers, since we use oour own storage and thus the preferred way to create containers is to create one container for each "unit of logical item" we

First we need to create an empty container:

```console
$ wl c create article-memex --owner cafe --storage-template cafe-dropbox \
   --title "As We May Think" \
   --category /data/type/article \
   --category /timeline/1945 \
   --category /topics/knowledge \
   --category /topics/notebooks
```

Now, we can mount it and fill with files:

```console
$ wl c mount article-memex
$ cp ~/Downloads/Memex.pdf ~/mnt/forests/cafe:/data/type/article/As\ We\ May\ Think/
```

Before publshing, please take a look how does the container look in the forest: do your proposed categories correspond well to those that are already in the Cafe forest? Did you make some spelling mistake or written timelime tag in a different format than others? Please correct any mistakes with:

```console
$ wl c edit article-memex
```
Now, look again. Once you're satisified, publish the container manifest to the Cafe forest, so that others could see it too:

```console
$ wl c publish article-memex
```

## Notes on access control

A Wildland container manifest can specify to which users its manifest should be encrypted. E.g. the following syntax will cause the container manifest to be encrypted to `someone` user's pubkey:

```yaml
owner: 0xcafe
(...)
access:
- user: 0xsomeone
```

But note that the manifest is _always_ encrypted automatically also to the container owner's pubkey. In case of a group user, such as `cafe` or `pandora`, this means encryption to all of the pubkeys which have been added (by admin) to the `cafe` group user. This is because the owner of all of the containers within the Cafe forest must be, by definition, the `cafe` group user, or otherwise the container is not presented within the forest's namespace.

This means that it is not possible to publish a container within a group forest, such as Cafe, with access set to a subset of the users -- **every user which is part of the `cafe` group user will always be able to decrypt any manifest, whose owner is `cafe`**.

Additionally, please note that manifest's encryption is not the same as container _content_ encryption. Right now the encryption backend, which implements the latter, has not been merged yet. This means that -- even though the Cafe forest is encrypted to a small group of users, the content of the containers is not encrypted. This will change once the encryption backend gets merged, but will require manual editing of the manifests.
