# Kartoteka Forest HOWTO

Kartoteka owner fingerprint:
```
0xb26e638663d445c1720168fffde90270157b7561f38f08cbbae9097c44c53fc9
```

This is internal-use-only HOWTO for the new "Kartoteka Forest". This internal forest is intended as our internal "root" directory, used for finding other forests, such as Cafe and Pandora.

The forest has been created via the new `wl forest create` command, using AWS S3 as storage for its manifest catalog container (formerly "infrastructure container"). The S3 bucket used for holding of the catalog is accessible only via s3:// URLs and not by any public http(s):// URLs. This is possible thanks to the recent change which introduced so called object links in place of simple URLs, allowing us to use any kind of storage we can use for container data also as manifests catalogue.

## Importing the user manifest

Since, kartoteka is intended to be our internal "root" directory, it must be bootstrapped by manually importing of its user manifest. The actual manifest can be found in the following public repo:

```
https://github.com/golemfoundation/wildland-bootstrap/
```

Note this manifest's storage section (pointing to the manifest catalogue) is encrypted to a group of users constituting the Wildland team (in practice this is the same group as those who are part of the Pandora group). This makes this forest private, even though it's manifest has been publicly exposed in the public repository above.

Thus, it is possible to import the manifest with the following command:

```console
$ wl u import --path /mydirs/kartoteka \
    https://raw.githubusercontent.com/golemfoundation/wildland-bootstrap/master/kartoteka.user.yaml
```

Note the `--path` option, which is used to tell at what Wildland paths should the bridge be created. These paths are at the discretion of the user and not of whoever created the kartoteka manifest. If this path is not given explicitly by the user, then the newly imported bridge will get `/forests/{UUID}-{transformed_first_path}` default path. It is intentional not to blindly copy-paste the paths from the user manifests, as these might be untrusted and whoever created the user manifest might want to trick the user.

The copy of the kartoteka manifest is also attached at the end of this HOWTO, thus it is possible to import it by copying it out first to a file, and then issuing:

```console
$ wl u import --path /mydirs/kartoteka kartoteka.user.yaml
```

After successful importing check for the new user. bridge and if the newly added user's pubkey matches that given at the beginning of this document:

```console
$ wl user ls # check the pubkey
$ wl bridge ls # check if the bridge has been created at desired paths
```

`TODO`: import using the hint syntax

## Mounting the forest

To mount kartoteka we can use the new simplified syntax:

```console
$ wl forest mount :/mydirs/kartoteka:
```

Note the `/mydirs/kartoteka` path should match whatever path you chose for the bridge created in the previous step during importing of the kartoteka user manifest.

Now, it should be possible to visit the forest of kartoteka:

```console
$ cd mnt/mydirs/kartoteka:
$ tree -L 1
```

The last command should show some of the bridges within kartoteka:

```
.
`-- forests/
    |-- cafe:/
    `-- pandora:/
```

To mount one of these forests:

```console
$ wl f mount :/mydirs/kartoteka:/forests/cafe:
```

Now it should be possible to visit this new forest entering to it via the `~/mnt/mydirs/kartoteka:/` directory!

Note how the Wildland addresses (e.g. `:/mydirs/kartoteka:/forests/cafe:`) match the FS-representation (e.g. `~/mnt/mydirs/kartoteka:/forests/cafe:`) -- this is an improvement thanks to the recently implemented convention to include `:` at the end of bridge-representing FS dirs.

## Creating local bridges for

It is now also possible to create a local bridge to one of the forests exposed via kartoteka and place them conveniently somewhere within own namespace -- this is mainly as a shortcut, e.g.:

```console
$ wl b import --path /work/pandora :/mydirs/kartoteka:/forests/pandora:
$ wl b import --path /fun/cafe :/mydirs/kartoteka:/forests/cafe:
```

Now it should be possible to mount any of these forests directly, without using kartoteka anymore:

```console
$ wl f mount :/work/pandora:
$ wl f mount :/fun/cafe:
```

## Copy of the kartoteka user manifest

```yaml
signature: |
  0xb26e638663d445c1720168fffde90270157b7561f38f08cbbae9097c44c53fc9:tmDVU4PX347/kkC4ijWQW2vP6dClrkVEmOfP0fUN8CxM9VmyYbqxRJdqv+XuzB1S395jv7BGVjY4TQcIdRMYCg==
---
object: user
owner: '0xb26e638663d445c1720168fffde90270157b7561f38f08cbbae9097c44c53fc9'
paths:
- /wildland/dirs/kartoteka
infrastructures:
- object: link
  file: /.manifests.yaml
  storage:
    encrypted:
      encrypted-data: I50s2TEA9KoQFivTPbW+Uqb0NgqLh/FrOfFV8+vvGqDEIb/dA/mXmckD9x4MwMRTZIbSJDYehTJ6CZhF8gj0A7loiydf3K60A3innFv/xvg5wJXgal0XkJjXe0cNLvt7Gw+epHLA3W32Ez1NWJ4cHSFeqyAH+4B0Iw5r8yo0e4GI0IXhDaj3uXbqtTRb9tPvwsAphnZUKYpqOcERGceukH0tphB9AIDwZX8Xk2HrexJCHl+P+SGXAO7SliclSPjg6p4uJrAPN+0mfYlwHmDiR7fF9sa64Q4jGf3d/StJl7CZALRHB1D6598iDKWANg1ZF8MzZGHmtZ5QVKaoTBYIHEjF8Yg8a/Vbk6x8rCXwsn7k02TSHh8yvgJ3mW+IrlvoTqeu6QA5ePrbzWbG3EV5M72PpvmxdypEWhfk+2oeqnsNKqNzjcvTmtZGXkxIU9hsb62J6oimf1vlhFjh1KUJrMGXhe2Xva//oglhNc6S7KF1STExr6Lrbvw8RcMt23xF0cnMmRXaO5PHRLvk66HEljRj3XSpTF7fmYl3ZTNpPXbux9abuDNlb7mSMv5BQzrxV3SFDifakY8sb2j7jthSDFxBIVStKAFbzpihm14+eMyA+B5UcbXaD7s0YFMA1dOadbNXVGb5/gQdfMMi6XZiVmst2pfynHWPnUkP4w+YmKjTqg7taywlbh+lHbx7Khl43L5w9XVndQXqOocGtqBA6aDPQmSjFXXrN1i8AiTgUHVL75SvrZALf3BS77c80YeR91P3v1Nb4tQRQggHczg1zxLYhVuhJLJXgRPKDcdO67bdTj09pSWSTgOSsliB0v17VnpCb6pdOxUMThKqtxG6Jg==
      encrypted-keys:
      - aoDVappwcJ6oLrhofizx6nogRiz28zkzs7XbzIU+ZwRmJAw+O4kUrqLo+5+5KMEWSyu3BF81yoTOI9YWm4wLsSr6Z5piBDtCaM8Foq2pwQ8=
      - hwWe9WhvGqFkVu7ijatWCtYjuhk8qW/EZ1eq0WngYh7c/8ihIDcf/oaa8rjHmLRY29xR3hcWGwg9YkBWUYkAbpUM8YVKlmrJWCfmz1b13WA=
      - mgyaonXztyaoBvBaTn6/j03Dw5WuAkiErDWMZyLQ8mN+XmyOkWawASDoDcKGfUjIyGd8rQNyXecXuJhNfyOOzyQNHDq2ndboRaNZcwIFMis=
      - 3PfGJouJLJqolny0/3qR1edGS7a5WfjeVaiJQYS0Y3vRPbvUbae8A25xLnGJez5oqNB+DtskGdGhBIThjtImP9bVgLIsOtKW5aRPTH6zk38=
      - QRO0U4ZNtS7nTmQVnS1eNwvryiHQsL1uNplejsA7ZVNAPtXQuzVSLmA8p7THEtev3ocrFvZtcBV+hqCB1CFa+ru4dcbwKzhNs811nhFhU2k=
      - q0ECH6M/GecytwVOI93LfH0ua+3rKGVYhFerOo7a20mKRUc/FWPXoHmiaoRW6cbD0GxxMM8rI9cS1IQlJVSgQAmTYeqkkgFQmnfMEgdF69I=
      - RsoJLzI4MmpvX6smo9r5LBTkgXvuH6+7TZAY30YmbyITUncAsPgRpmVaLBUQoN2jvlPkhlHniSn1yl5EOzupMZvUyoJy+v56+QRtOvfI+HA=
      - yt2fJtnTTEyxIczTrg4Qyyz8FHkPfN2E6bPpiIJxUlvj3E2fCRvfekjAQxI7Zy22VaO78IhnofFT4wlZrGZRHjdxDMfOjgxRBDRxsoh58aQ=
      - Jk9FOgKA1761ClaRt+ccb555KKq9Ug4VhKshu/WX5AEm/iY/QIiwE3C42FrJnJ9/8tcgEYBakRW8k6P1oKHYiUOGey76VH76hA06vXirNLs=
      - opUqE8TZP+4zo0p+AJC06X4UCIBrRjaefJFEBfrZ4242w5jRcJ/RSdgxfY6sQWT6bBidAbYvn2tmsBkooGhCDhbz0b7bfZ2ddRzNT0YnKu4=
      - lKWJagg7mnqan/OHPA/tlSL+lmXQ1FMiyXzmRm2HmT8yG3kLZeBsHmLPp9+B/sroJZN2R+0VYYmHcjreSp1IBCCKom6Jf6rPcB66tz3cc6Q=
      - H+Z+g3kNr5fjzaB85eObHLkuoHFjRIFlVw0HgjonC3D4bPlMsV1h1o+k0PZTDU1ywTnClC6uc4BLyHYLsJRVjH0xT2BtRMPqyBp7e/qNIq0=
      - StDJW/hAeAzL9IhExfQkcp0KtlQcUlTk0a3rQtT7YGvDfFfhZ3WqiUf+egRF5bI9/mes2BemHOdFV+Q2eCtC9sm+Ou+6l3hwk7+AAmbvhoM=
      - Zcu6qxnZfKywkdaClrOeyjov6E8qjv++MIrgRPlXmD8njbhxIrRvwh2Vtb0YNrvj90EYDaEGNDms+ShXIpFcgJCMf0R5Dpo4CUDt/M5x/qY=
      - qlWppNobNSPFibeXJW4jpJ0JRNC/Josp+jeEJH9II1M4p3wAlHtRpyUnEEnJRfUdUoYTkHJDjdJdUJ8ycT1ZhgrhXjc+2Bm1D+yqu51PYKU=
      - ddj+F6Xx8mwFE3UCSwoQyVorDNUmXBrNTDWzNhe5dlNr4CkudUZpWkH4jtMLktieDYAV5aynS7tiZn1q48ZlBJXedilsOool/VCkqL08FU0=
      - OKhcHwJ7vJ0NmtFXaCj2gk/hPxOI3rR/ozYT1RzGtyT0CVwp96g9kCmMbJTR7Izd514/w7uRYDKY2K8JSyKvNk0hFUSn+Ywk9iqrmBgYFuE=
pubkeys:
- RWSJHiWNxJ/f7bqx72HL5Vq3A/i8jN3LzH0hgH4lv61Wt35NNHXph26A0t436Gl2bi+XX7T6vmrNL3t5bKUhbuB0
version: '1'
```
