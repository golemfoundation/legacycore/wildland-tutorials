# Wildland Contributor Agreement

This Contributor Agreement (this **“Agreement”**) applies to any Contribution you make to any Work.

This is a binding legal agreement on you and any organization you represent. If you are signing this
Agreement on behalf of other organization, you represent and warrant that you have the authority to
agree to this Agreement on behalf of the organization.

## 1. Definitions

**“Contribution”** means any original work, including any modification of or addition to an existing work,
that you submit to Wildland project in any manner for inclusion in any Work.

**“Foundation”**, **“we”** and **“us”** means Golem Foundation Grafenauweg 8, 6300 Zug, Switzerland.

**“Work”** means any project, work or materials owned or managed by Foundation, and in particular
related to Wildland project.

**“You”** and **“your”** means you and any organization on whose behalf you are entering this Agreement.

## 2. Copyright Assignment, License and Waiver

**a.** **Assignment.** By submitting a Contribution, you assign to Golem all right, title and
interest in any copyright and other intellectual property rights you have in the
Contribution, and you waive any rights, including any moral rights, database rights,
etc., that may affect our ownership of the copyright in the Contribution.

**b.** **License to Foundation.** If your assignment in Section 2 a. is ineffective for any
reason, you grant to us and to any recipient of any Work distributed by us, a
perpetual, worldwide, transferable, non-exclusive, no-charge, royalty-free, irrevocable,
and sub-licensable license to use, reproduce, prepare derivative works of, publicly
display, publicly perform, sublicense, and distribute Contributions and any derivative
work created based on a Contribution. If your license grant is ineffective for any
reason, you irrevocably waive and covenant to not assert any claim you may have
against us, our successors in interest, and any of our direct or indirect licensees and
customers, arising out of our, our successors in interest’s, or any of our direct or
indirect licensees’ or customers’ use, reproduction, preparation of derivative works,
public display, public performance, sublicense, and distribution of a Contribution. You
also agree that we may publicly use your name and the name of any organization on
whose behalf you are entering into this Agreement in connection with publicizing the
Work.

## 3. Patent License

You grant to us and to any recipient of any Work distributed by us, a
perpetual, worldwide, transferable, non-exclusive, no-charge, royalty-free, irrevocable, and
sublicensable patent license to make, have made, use, sell, offer to sell, import, and otherwise
transfer the Contribution in whole or in part, alone or included, in any Work under any patent
you own, or license from a third party, that is necessarily infringed by the Contribution or by
combination of the contribution with any Work.

## 4. Your Representations and Warranties

By submitting a Contribution, you represent and
warrant that:

**a.** each Contribution you submit is an original work and you can legally (without any kind
of limitation) grant the rights set out in this Agreement;

**b.** the Contribution does not, and any exercise of the rights granted by you (in particular,
but without limitation, the licensing of the Contribution under GNU General Public
License v3 by Foundation) will not infringe any third party’s intellectual property or
other right. You explicitly acknowledge that Golem may license your Contribution
under the GNU General Public License v3. You represent and warrant that your
Contribution is free of any rights, obligations or restrictions which may conflict or
interfere with the terms of the GNU General Public License v3 and that Foundation
can, without any kind of limitation and without infringing any third party’s intellectual
property or other right, license your Contribution under the GNU General Public
License v3;

**c.** you are not aware of any claims, suits, or actions pertaining to and/or otherwise associated with the Contribution. You will notify us immediately if you become aware or
have reason to believe that any of your representations and warranties is or becomes
inaccurate. In this case, however, you are still bound by representations and
warranties made herein. Moreover, you represent and warrant that your Contribution
is free of any malicious content (e.g. virus, worm, Trojan horse, Easter egg, time
bomb, spyware, scareware, malware or other computer code, file or program that is or
is potentially harmful or invasive or intended to damage or hijack the operation of, or
to monitor the use of, any hardware, software or equipment) and that the Contribution
and its use (as well as your use according to Section 2 c. above) does not violate any
law.

## 5. Indemnification

You agree to release and to indemnify, defend and hold harmless the
Foundation and its subsidiaries and affiliates, as well as the members of governance bodies,
officers, directors, employees, shareholders, contractors and representatives of any of the
foregoing entities, from and against any and all losses, liabilities, expenses, damages, costs
(including attorneys’ fees and court costs) claims (in particular third party claims) or actions of
any kind whatsoever arising or resulting from your violation of this Agreement (in particular,
but without limitations, regarding your warranties and representations set out herein).

## 6. Intellectual Property

Except for the assignment and licenses set forth in this Agreement, this
Agreement does not transfer any right, title, or interest in any intellectual property right of
either party to the other. If you choose to provide us with suggestions, ideas for improvement,
recommendations, or other feedback, on any Work we may use your feedback without any
restriction or payment.

## 7. Miscellaneous

This Agreement shall be governed by and construed and interpreted in
accordance with the substantive laws of Switzerland, excluding the Swiss conflict of law rules.
The United Nations Convention for the International Sales of Goods (**"Vienna Sales
Convention"**) is excluded. Any dispute under this Agreement shall be submitted to the
exclusive jurisdiction of the Courts of Zug, Switzerland.

This Agreement does not create a partnership, agency relationship, or joint venture be-tween
the parties. We may assign this Agreement without notice or restriction. You are not allowed to
assign this Agreement and/or any rights and obligations set out herein without our prior explicit
consent.

If any provision of this Agreement is unenforceable, that provision will be modified to ren-der it
enforceable to the extent possible to effect the parties’ intention and the remaining provisions
will not be affected. The parties may amend this Agreement only in a written amendment
signed by both parties. This Agreement comprises the parties’ entire agreement relating to the
subject matter of this Agreement.
