# Wildland Tutorials

Sources for the Legacy Wildland documentation site.

## Target audience

Computer-literate user, who understands what a file, directory and file manager is.

Some more advanced material might target **power users**, i.e. those who can write own shell scripts and create and run own Dockers.

### Copyright

© 2023 Golem Foundation, Zug, Switzerland
